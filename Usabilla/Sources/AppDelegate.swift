//
//  Copyright © 2019 Smirnov Maxim. All rights reserved.
//

import GoogleMaps
import RxFeedback
import RxSwift
import UIKit
import RxCocoa

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private let disposeBag = DisposeBag()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        GMSServices.provideAPIKey("AIzaSyAGUlFmRrNMIkr2DpVX5nOs5xVxDQIU5Z4")

        UINavigationBar.appearance().setDefault()
        let vc = ControllerFactory.makeMapController
        let nc = UINavigationController(rootViewController: vc)

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = nc
        window?.makeKeyAndVisible()

        makeSystem(navigationController: nc,
                   uiFeedback: vc.uiFeedback)
            .subscribe()
            .disposed(by: disposeBag)

        return true
    }

    private func makeSystem(navigationController: UINavigationController,
                            uiFeedback: @escaping FeedbackLoop) -> Observable<State> {
        let coordinator = Coordinator(navigationController: navigationController,
                                      controllerFactory: ControllerFactory.self)
        let systemFeedback =
            SideEffects(coordinator: coordinator,
                             service: Service(networkClient: NetworkClient(),
                                                    converter: ResponseConverter())).feedbackLoops
        return Observable.system(initialState: State(),
                                 reduce: State.reduce,
                                 scheduler: MainScheduler.instance,
                                 feedback: systemFeedback + [uiFeedback])
    }
}
