//
// Copyright © 2018 Smirnov Maxim. All rights reserved. 
//

import Foundation
import RxSwift

enum NetworkError: Error, Equatable {
    case unknown
}

struct NetworkClient {
    func performGet<T: Decodable>(endpoint: URL,
                                  for type: T.Type) -> Single<T> {

        return Single<T>.create { single in

            URLSession.shared.dataTask(with: endpoint) { (data, response, error) in
                if error != nil {
                    single(.error(NetworkError.unknown))
                    return
                }

                guard
                    let data = data,
                    let response = try? JSONDecoder().decode(T.self, from: data) else {
                        single(.error(NetworkError.unknown))
                        return
                }
                single(.success(response))
                }
                .resume()

            return Disposables.create()
        }
    }
}
