//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import CoreLocation

struct ResponseConverter {
    func convert(response: Response) -> [Feedback] {
        return response.items.compactMap(Feedback.init)
    }
}

private extension Feedback {
    init?(from item: Item) {

        guard
            let stringKind = item.labels.first,
            let kind = Feedback.Kind(rawValue: stringKind),
            let browser = Browser(rawValue: item.computed_browser.Browser.lowercased()) else {
                return nil
        }

        self.init(browser: browser,
                  comment: item.comment,
                  email: item.email,
                  kind: kind,
                  location: .init(coordinates: .init(latitude: item.geo.lat,
                                                     longitude: item.geo.lon),
                                  name: item.computed_location),
                  url: item.url)
    }
}
