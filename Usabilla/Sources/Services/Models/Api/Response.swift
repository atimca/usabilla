//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation

struct Response: Codable {
    let items: [Item]
}

struct Item: Codable {
    let computed_browser: ComputedBrowser
    let computed_location: String
    let labels: [String]
    let email: String?
    let geo: Geo
    let url: String
    let comment: String
}

struct ComputedBrowser: Codable {
    let Browser: String
}

struct Geo: Codable {
    let lat: Double
    let lon: Double
}
