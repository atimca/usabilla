//
//  Copyright © 2019 atimca. All rights reserved.
//

import CoreLocation

struct Feedback: Equatable {
    let browser: Browser
    let comment: String
    let email: String?
    let kind: Kind
    let location: Location
    let url: String
}

extension Feedback {

    struct Location: Equatable {
        let coordinates: CLLocationCoordinate2D
        let name: String
    }

    enum Kind: String {
        case bug
        case compliment
        case question
        case suggestion
    }
}
