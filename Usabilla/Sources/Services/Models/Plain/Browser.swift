//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

enum Browser: String {
    case chrome
    case firefox
    case ie
    case safari
    case unknown
}
