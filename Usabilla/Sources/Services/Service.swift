//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation
import RxSwift

struct Service {

    private let networkClient: NetworkClient
    private let converter: ResponseConverter

    init(networkClient: NetworkClient, converter: ResponseConverter) {
        self.networkClient = networkClient
        self.converter = converter
    }
}

extension Service {
    func getData() -> Single<[Feedback]> {
        return networkClient.performGet(endpoint: Constants.endpoint,
                                        for: Response.self)
            .map(converter.convert)
    }
}

private extension Service {
    enum Constants {
        static let endpoint = URL(string: "http://cache.usabilla.com/example/apidemo.json")!
    }
}
