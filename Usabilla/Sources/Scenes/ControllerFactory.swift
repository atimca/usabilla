//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import UIKit

enum ControllerFactory {
    static var makeMapController: MapViewController {
        return MapViewController()
    }

    static func makeDetailsController(with feedback: Feedback) -> UIViewController {
        return DetailsViewController(feedback: feedback)
    }
}
