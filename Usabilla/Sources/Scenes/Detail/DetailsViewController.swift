//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import RxCocoa
import RxSwift
import UIKit

class DetailsViewController: UIViewController {

    // MARK: - Properties
    private let viewState: ViewState

    // MARK: - UI Componets
    private let textView: UITextView = {
        let view = UITextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.font = .systemFont(ofSize: 17)
        view.textColor = .white
        view.isEditable = false
        view.dataDetectorTypes = .all
        view.contentInset = .init(top: 0, left: 16, bottom: 0, right: 16)
        return view
    }()

    // MARK: - Life Cycle

    init(feedback: Feedback) {
        self.viewState = .init(from: feedback)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUi()
        render(with: viewState)
    }

    private func render(with state: ViewState) {
        title = state.title
        textView.text = state.text
    }

    private func setupUi() {
        view.backgroundColor = .black
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.setDefault()
        view.addSubview(textView)
        textView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        textView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        textView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        textView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
}
