//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation

extension DetailsViewController {
    struct ViewState {
        let title: String
        let text: String
    }
}

extension DetailsViewController.ViewState {
    init(from feedback: Feedback) {
        let text =
        """
        Browser: \(feedback.browser.rawValue.capitalizedFirstLetter)
        Location: \(feedback.location.name.capitalizedFirstLetter)
        Email: \(feedback.email ?? "unknown")
        URL: \(feedback.url)

        Comment:
        \(feedback.comment)
        """
        self.init(title: feedback.kind.rawValue.capitalizedFirstLetter,
                  text: text)
    }
}
