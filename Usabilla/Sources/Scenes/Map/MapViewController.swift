//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import GoogleMaps
import RxFeedback
import RxSwift
import UIKit
import RxGoogleMaps

class MapViewController: UIViewController {

    // MARK: - UI Components

    private let mapView: GMSMapView = {
        let map = GMSMapView()
        // swiftlint:disable force_try
        let styleURL = Bundle.main.url(forResource: "style", withExtension: "json")!
        map.mapStyle = try! GMSMapStyle(contentsOfFileURL: styleURL)
        // swiftlint:enable force_try
        return map
    }()

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Life Cycle

    override func loadView() {
        view = mapView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Main Screen"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

// MARK: - DataDriven
extension MapViewController {
    func render(state: ViewState) {
        state.points.forEach { feedback in
            let marker = GMSMarker(position: feedback.location)
            marker.map = mapView
            marker.icon = feedback.icon
            marker.title = feedback.title
            marker.snippet = "Tap for more info"
        }
    }
}

// MARK: - Bindings
extension MapViewController {
    var uiFeedback: FeedbackLoop {
        return bind(self) { vc, state in
            let viewState = state
                .map { $0.feedbacks }
                .distinctUntilChanged()
                .map(ViewState.convert)
                .asDriver(onErrorDriveWith: .empty())

            let subscriptions = [
                viewState
                    .drive(onNext: { vc.render(state: $0) })
            ]

            let events: [Observable<Event>] = [
                vc.mapView.rx.didTapInfoWindowOf
                    .map { .feedbackSelected(location: $0.position) }
            ]
            return Bindings(subscriptions: subscriptions, events: events)
        }
    }
}
