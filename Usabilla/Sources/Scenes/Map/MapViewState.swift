//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import CoreLocation
import UIKit

struct PointViewState {
    let icon: UIImage
    let location: CLLocationCoordinate2D
    let title: String
}

extension MapViewController {
    struct ViewState {
        let points: [PointViewState]

        static func convert(from feedbacks: [Feedback]) -> ViewState {
            return ViewState(points: feedbacks.map(PointViewState.init))
        }
    }
}

private extension PointViewState {
    init(from feedback: Feedback) {
        self.init(icon: feedback.kind.icon,
                  location: feedback.location.coordinates,
                  title: feedback.browser.rawValue)
    }
}

private extension Feedback.Kind {
    var icon: UIImage {
        switch self {
        case .bug:
            return #imageLiteral(resourceName: "bug_icon")
        case .compliment:
            return #imageLiteral(resourceName: "compliment_icon")
        case .question:
            return #imageLiteral(resourceName: "question_icon")
        case .suggestion:
            return #imageLiteral(resourceName: "idea_icon")
        }
    }
}
