//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import UIKit

struct Coordinator {

    private let navigationController: UINavigationController
    private let controllerFactory: ControllerFactory.Type

    public init(navigationController: UINavigationController,
                controllerFactory: ControllerFactory.Type) {
        self.navigationController = navigationController
        self.controllerFactory = controllerFactory
    }

    func showDetails(with feedback: Feedback) {
        navigationController
            .pushViewController(controllerFactory.makeDetailsController(with: feedback),
                                animated: true)
    }
}
