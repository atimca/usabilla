//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import CoreLocation

struct State: Equatable {
    var feedbacks: [Feedback] = []
    var selectedFeedback: Feedback?
}

enum Event {
    case feedbackSelected(location: CLLocationCoordinate2D)
    case feedbacksWereDownloaded([Feedback])
    case detailsLoaded
}

// MARK: - Query
extension State {
    var queryShouldDownloadFeedbacks: Bool {
        guard feedbacks.isEmpty else { return false }
        return true
    }
}

// MARK: - Reducer
extension State {
    static func reduce(state: State, event: Event) -> State {
        var result = state
        switch event {
        case .feedbackSelected(let location):
            result.selectedFeedback = result.feedbacks.first { $0.location.coordinates == location }
        case .feedbacksWereDownloaded(let feedbacks):
            result.feedbacks = feedbacks
        case .detailsLoaded:
            result.selectedFeedback = nil
        }
        return result
    }
}
