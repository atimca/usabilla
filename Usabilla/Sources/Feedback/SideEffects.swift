//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import RxSwift
import RxFeedback

typealias FeedbackLoop = (ObservableSchedulerContext<State>) -> Observable<Event>

struct SideEffects {

    private let coordinator: Coordinator
    private let service: Service

    public init(coordinator: Coordinator, service: Service) {
        self.coordinator = coordinator
        self.service = service
    }
}

extension SideEffects {
    var feedbackLoops: [FeedbackLoop] {
        return [
            react(request: { $0.selectedFeedback }, effects: openDetailsScene),
            react(request: { $0.queryShouldDownloadFeedbacks }, effects: download)
        ]
    }

    var openDetailsScene: (Feedback) -> Observable<Event> {
        return {
            self.coordinator.showDetails(with: $0)
            return .just(.detailsLoaded)
        }
    }

    var download: (Bool) -> Observable<Event> {
        return {
            guard $0 else { return .empty() }
            return self.service.getData()
                .map { .feedbacksWereDownloaded($0) }
                .catchError({ _ in return .just(.feedbacksWereDownloaded([])) })
                .asObservable()
        }
    }
}
