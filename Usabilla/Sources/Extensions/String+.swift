//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

extension String {
    var capitalizedFirstLetter: String {
        return prefix(1).uppercased() + lowercased().dropFirst()
    }
}
