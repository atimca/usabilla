# Usabilla

There are also three examples of my code:  
1) [With architectural approach and clean code](https://github.com/Atimca/Currencies)  
2) [Regarding to one screen task with the `KISS` principle](https://github.com/Atimca/PhotoGallery)  
3) [Just a task written on Objective-C](https://github.com/Atimca/EmployeeSalaries)

**Tech stack:**

- Plain `URLSession` for networking
- `DataDriven` pattern for working with views
- `RxFeedback` acrhitecture pattern as base

**Disclamer:**  

I've created a map with feedback points. You can tap on it and see some info. If you tap on the info you go to the detail screen.

I completely understand that RxFeedback is some kind of overengineering for this task. However, I wanted this test to be a demonstration of abilities and not just the simplest solution for the app that only contains two screens. I didn't do proper testing, because you can see it in another example.
PS: I've designed it by myself and added Sketch file ;)